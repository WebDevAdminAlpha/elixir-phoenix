defmodule ElixirPhoenixWeb.PageController do
  use ElixirPhoenixWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  # "Unsafe `binary_to_term`"
  def bin_to_term_test(_conn, %{"test" => test}) do
    :erlang.binary_to_term(test)
  end

  # "vulnerable send_download"
  def traversal_send_download(conn, %{"test" => test}) do
    send_download(conn, {:file, test})
  end

  # "vulnerable send_file"
  def traversal_send_file(conn, %{"test" => test}) do
    send_file(conn, 200, test)
  end

  # "SQL injection in `SQL.stream`"
  def sql_stream(%{"sql" => sql}) do
    SQL.stream(Repo, sql, [])
  end

  # "SQL injection in `SQL`"
  def sql_query(%{"sql" => sql}) do
    SQL.query(Repo, sql, [])
  end

  # "Unsafe atom interpolation"
  def dos_binary_to_atom(conn, %{"test" => test}) do
    render conn, :"foo#{test}"
  end

  # "Command Injection in `System.cmd`"
  def ci_system(_conn, %{"test" => test}) do
    System.cmd(test, [])
  end

  # "Command Injection in `:os.cmd`"
  def ci_os(_conn, %{"test" => test}) do
    :os.cmd(test, [])
  end

  # "Unsafe `List.to_atom`"
  def list_to_atom(conn, %{"test" => test}) do
    render conn, List.to_atom(test)
  end

  # "Unsafe indirect `String.to_atom`"
  def string_to_atom(conn, params) do
    render conn, String.to_atom(params["test"])
  end

  # "Code Execution in Code functions"
  def code_exec(eval_input) do
    Code.eval_string(eval_input)
    Code.eval_file(eval_input)
    Code.eval_quoted(eval_input)
  end

  # "Code Execution in EEx functions"
  def eex_code_exec(eval_input) do
    EEx.eval_string(eval_input)
    EEx.eval_file(eval_input)
  end


  # "Traversal in File functions first parameter"
  def file_traversal(file_name) do
    File.read(file_name)
    File.read!(file_name)
    File.rm(file_name)
    File.rm!(file_name)
    File.rm_rf(file_name)
    File.write(file_name, "data")
    File.write!(file_name, "data")
    File.cp("file.txt", file_name)
    File.cp!("file.txt", file_name)
    File.cp_r("file.txt", file_name)
    File.cp_r!("file.txt", file_name)
    File.ln("file.txt", file_name)
    File.ln!("file.txt", file_name)
    File.ln_s("file.txt", file_name)
    File.ln_s!("file.txt", file_name)
  end

  # "vulnerable send_download"
  def vulnerable_send_download(conn, %{"test" => test}) do
    send_download conn, {:file, test}
  end
end
