defmodule ElixirPhoenixWeb.Router do
  use ElixirPhoenixWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    # Disable CSRF protections.
    # raises no error as Config module is not enabled
    # plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ElixirPhoenixWeb do
    pipe_through :browser

    get "/",                        PageController, :index
    get "/bin_to_term_test",        PageController, :bin_to_term_test
    get "/ci_system",               PageController, :ci_system
    get "/dos_binary_to_atom",      PageController, :dos_binary_to_atom
    get "/sql_query",               PageController, :sql_query
    get "/traversal_send_download", PageController, :traversal_send_download

    get "/xss", XssController, :xss_aliased_put_content_type
    get "/xss", XssController, :xss_put_content_type
  end

  # Other scopes may use custom stacks.
  # scope "/api", ElixirPhoenixWeb do
  #   pipe_through :api
  # end
end
